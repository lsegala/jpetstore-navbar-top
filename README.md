Project setup complete!
Steps to test your React single-spa application:

1. Run 'npm start -- --port 9300'
2. Go to http://single-spa-playground.org/playground/instant-test?name=@lsegala/content&url=9300 to see it working!
