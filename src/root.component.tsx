import React from "react";

export default function Root(props) {
  return (
    <section>
      <style>
        @import
        url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
      </style>
      <img
        src="https://iconorbit.com/icons/256-watermark/1711201506491787455-Parrot.jpg"
        style={{ float: "left" }}
      />
      <div
        style={{
          fontFamily: "Anton, sans-serif",
          textAlign: "center",
          width: "100%",
          fontSize: "28px",
          paddingTop: "3em",
        }}
      >
        JPetStore Demo (Microfrontend)
      </div>
    </section>
  );
}
